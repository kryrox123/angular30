import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-div',
  templateUrl: './div.component.html',
  styleUrls: ['./div.component.css']
})
export class DivComponent implements OnInit {

  altura: number = 30
  txt: string = "";
  mensaje_advertencia: string = "límite"
  constructor() { }

  ngOnInit(): void {

  }

  aumentar(){
    this.altura = this.altura + 5
    if (this.altura === 500){
      this.txt = this.mensaje_advertencia
    }
  }

  disminuir(){
    this.altura = this.altura - 5;
    if(this.altura === 5){
      this.txt = this.mensaje_advertencia
    }
  }
}
